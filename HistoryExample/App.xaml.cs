﻿// -----------------------------------------------------------------------
// <copyright file="App.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample
{
    using System.Windows;

    /// <summary>
    ///     Interaction logic for the application.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        ///     Gets the view model instance.
        /// </summary>
        public static ViewModel.ViewModel ViewModel { get; } = new ViewModel.ViewModel();
    }
}