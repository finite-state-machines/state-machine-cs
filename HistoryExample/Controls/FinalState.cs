﻿// -----------------------------------------------------------------------
// <copyright file="FinalState.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows;

    /// <summary>
    ///     Draws a final state.
    /// </summary>
    /// <seealso cref="HistoryExample.Controls.State" />
    public class FinalState : State
    {
        /// <summary>
        ///     Initializes static members of the <see cref="FinalState" /> class.
        /// </summary>
        static FinalState() =>
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(FinalState),
                new FrameworkPropertyMetadata(typeof(FinalState)));

        /// <summary>
        ///     Initializes a new instance of the <see cref="FinalState" /> class.
        /// </summary>
        public FinalState() => this.Id = StateMachine.State.FinalStateName;
    }
}