﻿// -----------------------------------------------------------------------
// <copyright file="InitialState.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows;

    /// <summary>
    ///     Draws an initial state.
    /// </summary>
    /// <seealso cref="HistoryExample.Controls.State" />
    public class InitialState : State
    {
        /// <summary>
        ///     Initializes static members of the <see cref="InitialState" /> class.
        /// </summary>
        static InitialState() =>
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(InitialState),
                new FrameworkPropertyMetadata(typeof(InitialState)));

        /// <summary>
        ///     Initializes a new instance of the <see cref="InitialState" /> class.
        /// </summary>
        public InitialState() => this.Id = StateMachine.State.InitialStateName;
    }
}