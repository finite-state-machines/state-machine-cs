﻿// -----------------------------------------------------------------------
// <copyright file="L2PreparingControl.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for L2PreparingControl.
    /// </summary>
    public partial class L2PreparingControl : UserControl
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="L2PreparingControl" /> class.
        /// </summary>
        public L2PreparingControl() => this.InitializeComponent();
    }
}