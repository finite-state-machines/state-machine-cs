﻿// -----------------------------------------------------------------------
// <copyright file="L2WorkingControl.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for L2WorkingControl.
    /// </summary>
    public partial class L2WorkingControl : UserControl
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="L2WorkingControl" /> class.
        /// </summary>
        public L2WorkingControl() => this.InitializeComponent();
    }
}