﻿// -----------------------------------------------------------------------
// <copyright file="MainControl.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for MainControl.
    /// </summary>
    public partial class MainControl : UserControl
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="MainControl" /> class.
        /// </summary>
        public MainControl() => this.InitializeComponent();
    }
}