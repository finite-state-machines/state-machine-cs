﻿// -----------------------------------------------------------------------
// <copyright file="WorkingControl.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Controls
{
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for WorkingControl.
    /// </summary>
    public partial class WorkingControl : UserControl
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="WorkingControl" /> class.
        /// </summary>
        public WorkingControl() => this.InitializeComponent();
    }
}