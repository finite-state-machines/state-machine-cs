﻿// -----------------------------------------------------------------------
// <copyright file="Events.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Model
{
    using StateMachine;

    /// <summary>
    ///     A class containing all events used by the state machines of this application.
    /// </summary>
    public static class Events
    {
        /// <summary>
        ///     The break event.
        /// </summary>
        public static readonly FsmEvent Break = new FsmEvent(nameof(Events.Break));

        /// <summary>
        ///     The continue event for flat history.
        /// </summary>
        public static readonly FsmEvent Continue = new FsmEvent(nameof(Events.Continue));

        /// <summary>
        ///     The continue event for deep history.
        /// </summary>
        public static readonly FsmEvent ContinueDeep = new FsmEvent(nameof(Events.ContinueDeep));

        /// <summary>
        ///     The next event.
        /// </summary>
        public static readonly FsmEvent Next = new FsmEvent(nameof(Events.Next));

        /// <summary>
        ///     The restart event.
        /// </summary>
        public static readonly FsmEvent Restart = new FsmEvent(nameof(Events.Restart));
    }
}