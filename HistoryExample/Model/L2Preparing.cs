﻿// -----------------------------------------------------------------------
// <copyright file="L2Preparing.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Model
{
    using System.Diagnostics;
    using StateMachine;

    /// <summary>
    ///     The composite state machine of the L2-Preparing state.
    /// </summary>
    public class L2Preparing
    {
        /// <summary>
        ///     The name of the state p1.
        /// </summary>
        public readonly State<object> StateL3P1 = new State<object>("L3-P1");

        /// <summary>
        ///     The name of the state p2.
        /// </summary>
        public readonly State<object> StateL3P2 = new State<object>("L3-P2");

        /// <summary>
        ///     The name of the state p3.
        /// </summary>
        public readonly State<object> StateL3P3 = new State<object>("L3-P3");

        /// <summary>
        ///     The name of the state p4.
        /// </summary>
        public readonly State<object> StateL3P4 = new State<object>("L3-P4");

        /// <summary>
        ///     Initializes a new instance of the <see cref="L2Preparing" /> class.
        /// </summary>
        public L2Preparing() => this.Initialize();

        /// <summary>
        ///     Gets the embedded state machine.
        /// </summary>
        public Fsm<object> Machine { get; } = new Fsm<object>("L2-Preparing");

        /// <summary>
        ///     Initializes the state machine.
        /// </summary>
        private void Initialize()
        {
            this.Machine.Initial
                .Transition(FsmEvent.NoEvent, this.StateL3P1);

            this.StateL3P1
                .Transition(Events.Next, this.StateL3P2)
                .Entry(this.P1Entry);

            this.StateL3P2
                .Transition(Events.Next, this.StateL3P3)
                .Entry(this.P2Entry);

            this.StateL3P3
                .Transition(Events.Next, this.StateL3P4)
                .Entry(this.P3Entry);

            this.StateL3P4
                .Transition(Events.Next, this.Machine.Final)
                .Entry(this.P4Entry);
        }

        /// <summary>
        ///     Handles the entry action of the P1 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void P1Entry(object data) => Debug.WriteLine(nameof(this.P1Entry));

        /// <summary>
        ///     Handles the entry action of the P2 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void P2Entry(object data) => Debug.WriteLine(nameof(this.P2Entry));

        /// <summary>
        ///     Handles the entry action of the P3 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void P3Entry(object data) => Debug.WriteLine(nameof(this.P3Entry));

        /// <summary>
        ///     Handles the entry action of the P4 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void P4Entry(object data) => Debug.WriteLine(nameof(this.P4Entry));
    }
}