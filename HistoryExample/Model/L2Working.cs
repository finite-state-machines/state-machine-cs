﻿// -----------------------------------------------------------------------
// <copyright file="L2Working.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Model
{
    using System.Diagnostics;
    using StateMachine;

    /// <summary>
    ///     The composite state machine of the L2-Working state.
    /// </summary>
    public class L2Working
    {
        /// <summary>
        ///     The name of the state w1.
        /// </summary>
        public readonly State<object> StateL3W1 = new State<object>("L3-W1");

        /// <summary>
        ///     The name of the state w2.
        /// </summary>
        public readonly State<object> StateL3W2 = new State<object>("L3-W2");

        /// <summary>
        ///     The name of the state w3.
        /// </summary>
        public readonly State<object> StateL3W3 = new State<object>("L3-W3");

        /// <summary>
        ///     The name of the state w4.
        /// </summary>
        public readonly State<object> StateL3W4 = new State<object>("L3-W4");

        /// <summary>
        ///     The name of the state w5.
        /// </summary>
        public readonly State<object> StateL3W5 = new State<object>("L3-W5");

        /// <summary>
        ///     Initializes a new instance of the <see cref="L2Working" /> class.
        /// </summary>
        public L2Working() => this.Initialize();

        /// <summary>
        ///     Gets the embedded state machine.
        /// </summary>
        public Fsm<object> Machine { get; } = new Fsm<object>("L2-Working");

        /// <summary>
        ///     Initializes the state machine.
        /// </summary>
        private void Initialize()
        {
            this.Machine.Initial
                .Transition(FsmEvent.NoEvent, this.StateL3W1);

            this.StateL3W1
                .Transition(Events.Next, this.StateL3W2)
                .Entry(this.W1Entry);

            this.StateL3W2
                .Transition(Events.Next, this.StateL3W3)
                .Entry(this.W2Entry);

            this.StateL3W3
                .Transition(Events.Next, this.StateL3W4)
                .Entry(this.W3Entry);

            this.StateL3W4
                .Transition(Events.Next, this.StateL3W5)
                .Entry(this.W4Entry);

            this.StateL3W5
                .Transition(Events.Next, this.Machine.Final)
                .Entry(this.W5Entry);
        }

        /// <summary>
        ///     Handles the entry action of the W5 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void W5Entry(object data) => Debug.WriteLine(nameof(this.W5Entry));

        /// <summary>
        ///     Handles the entry action of the W4 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void W4Entry(object data) => Debug.WriteLine(nameof(this.W4Entry));

        /// <summary>
        ///     Handles the entry action of the W3 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void W3Entry(object data) => Debug.WriteLine(nameof(this.W3Entry));

        /// <summary>
        ///     Handles the entry action of the W2 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void W2Entry(object data) => Debug.WriteLine(nameof(this.W2Entry));

        /// <summary>
        ///     Handles the entry action of the W1 state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void W1Entry(object data) => Debug.WriteLine(nameof(this.W1Entry));
    }
}