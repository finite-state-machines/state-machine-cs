﻿// -----------------------------------------------------------------------
// <copyright file="Main.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Model
{
    using System.Diagnostics;
    using StateMachine;

    /// <summary>
    ///     The main state machine.
    /// </summary>
    public class Main
    {
        /// <summary>
        ///     The name of the state initializing.
        /// </summary>
        public readonly State<object> StateInitializing = new State<object>("Main-Initializing");

        /// <summary>
        ///     The name of the state working.
        /// </summary>
        public readonly State<object> StateWorking = new State<object>("Main-Working");

        /// <summary>
        ///     The name of the state handling error.
        /// </summary>
        public readonly State<object> StateHandlingError = new State<object>("Main-HandlingError");

        /// <summary>
        ///     The name of the state finalizing.
        /// </summary>
        public readonly State<object> StateFinalizing = new State<object>("Main-Finalizing");

        /// <summary>
        ///     The child state machine of the working state.
        /// </summary>
        public readonly Working Working = new Working();

        /// <summary>
        ///     Initializes a new instance of the <see cref="Main" /> class.
        /// </summary>
        public Main() => this.Initialize();

        /// <summary>
        ///     Gets the embedded state machine.
        /// </summary>
        public Fsm<object> Machine { get; } = new Fsm<object>("Main");

        /// <summary>
        ///     Starts the behavior of the state machine.
        /// </summary>
        public void Start() => this.Machine.Start(null);

        /// <summary>
        ///     Triggers the specified event.
        /// </summary>
        /// <param name="event">The trigger.</param>
        public void Trigger(FsmEvent @event) => this.Machine.Trigger(@event, null);

        /// <summary>
        ///     Initializes the state machine.
        /// </summary>
        private void Initialize()
        {
            this.Machine.Initial
                .Transition(FsmEvent.NoEvent, this.StateInitializing);

            this.StateInitializing
                .Transition(Events.Next, this.StateWorking);

            this.StateWorking
                .Transition(Events.Break, this.StateHandlingError)
                .Transition(FsmEvent.NoEvent, this.StateFinalizing)
                .Entry(this.WorkingEntry)
                .Child(this.Working.Machine);

            this.StateHandlingError
                .Transition(Events.Restart, this.StateWorking)
                .Transition(Events.Continue, this.StateWorking.History)
                .Transition(Events.ContinueDeep, this.StateWorking.DeepHistory)
                .Transition(Events.Next, this.StateFinalizing)
                .Transition(Events.Break, this.Machine.Final)
                .Entry(this.HandlingErrorEntry);

            this.StateFinalizing
                .Transition(Events.Next, this.StateInitializing)
                .Entry(this.FinalizingEntry);
        }

        /// <summary>
        ///     Handles the entry action of the Finalizing state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void FinalizingEntry(object data) => Debug.WriteLine(nameof(this.FinalizingEntry));

        /// <summary>
        ///     Handles the entry action of the HandlingError state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void HandlingErrorEntry(object data) => Debug.WriteLine(nameof(this.HandlingErrorEntry));

        /// <summary>
        ///     Handles the entry action of the Working state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void WorkingEntry(object data) => Debug.WriteLine(nameof(this.WorkingEntry));
    }
}