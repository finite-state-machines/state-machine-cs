﻿// -----------------------------------------------------------------------
// <copyright file="Working.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.Model
{
    using System.Diagnostics;
    using StateMachine;

    /// <summary>
    ///     The composite state machine of the working state.
    /// </summary>
    public class Working
    {
        /// <summary>
        ///     The name of the state initializing.
        /// </summary>
        public readonly State<object> StateL2Initializing = new State<object>("L2-Initializing");

        /// <summary>
        ///     The name of the state preparing.
        /// </summary>
        public readonly State<object> StateL2Preparing = new State<object>("L2-Preparing");

        /// <summary>
        ///     The name of the state working.
        /// </summary>
        public readonly State<object> StateL2Working = new State<object>("L2-Working");

        /// <summary>
        ///     The child state machine of the preparing state.
        /// </summary>
        public readonly L2Preparing L2Preparing = new L2Preparing();

        /// <summary>
        ///     The child state machine of the working state.
        /// </summary>
        public readonly L2Working L2Working = new L2Working();

        /// <summary>
        ///     Initializes a new instance of the <see cref="Working" /> class.
        /// </summary>
        public Working() => this.Initialize();

        /// <summary>
        ///     Gets the embedded state machine.
        /// </summary>
        public Fsm<object> Machine { get; } = new Fsm<object>("Working");

        /// <summary>
        ///     Initializes the state machine.
        /// </summary>
        private void Initialize()
        {
            this.Machine.Initial
                .Transition(FsmEvent.NoEvent, this.StateL2Initializing);

            this.StateL2Initializing
                .Transition(Events.Next, this.StateL2Preparing)
                .Entry(this.L2InitializingEntry);

            this.StateL2Preparing
                .Transition(FsmEvent.NoEvent, this.StateL2Working)
                .Entry(this.L2PreparingEntry)
                .Child(this.L2Preparing.Machine);

            this.StateL2Working
                .Transition(FsmEvent.NoEvent, this.Machine.Final)
                .Entry(this.L2WorkingEntry)
                .Child(this.L2Working.Machine);
        }

        /// <summary>
        ///     Handles the entry action of the Initializing state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void L2InitializingEntry(object data) => Debug.WriteLine(nameof(this.L2InitializingEntry));

        /// <summary>
        ///     Handles the entry action of the Preparing state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void L2PreparingEntry(object data) => Debug.WriteLine(nameof(this.L2PreparingEntry));

        /// <summary>
        ///     Handles the entry action of the Working state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void L2WorkingEntry(object data) => Debug.WriteLine(nameof(this.L2WorkingEntry));
    }
}