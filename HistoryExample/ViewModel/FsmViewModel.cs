﻿// -----------------------------------------------------------------------
// <copyright file="FsmViewModel.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace HistoryExample.ViewModel
{
    using Common.Mvvm;
    using StateMachine;

    /// <summary>
    ///     The view model for the state machines.
    /// </summary>
    /// <seealso cref="Common.Mvvm.ModelBase" />
    public class FsmViewModel : ModelBase
    {
        /// <summary>
        ///     The associated state machine.
        /// </summary>
        private readonly Fsm<object> fsm;

        /// <summary>
        ///     The current state.
        /// </summary>
        private string currentState;

        /// <summary>
        ///     A value indicating whether the state machine is inactive.
        /// </summary>
        private bool isInactive = true;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FsmViewModel" /> class.
        /// </summary>
        /// <param name="fsm">The state machine to associate.</param>
        public FsmViewModel(Fsm<object> fsm)
        {
            this.fsm = fsm;
            this.fsm.StateChanged += this.OnStateChanged;
            this.CurrentState = this.fsm.CurrentState.Name;
        }

        /// <summary>
        ///     Gets or sets the current state.
        /// </summary>
        public string CurrentState
        {
            get => this.currentState;
            set
            {
                this.SetField(ref this.currentState, value);
                this.IsInactive = this.fsm.CurrentState == this.fsm.Initial ||
                                  this.fsm.HasFinished;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the state machine is inactive.
        /// </summary>
        public bool IsInactive
        {
            get => this.isInactive;
            set => this.SetField(ref this.isInactive, value);
        }

        /// <summary>
        ///     Called when the state machine changes it's state.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="StateChangedEventArgs" /> instance containing the event data.</param>
        private void OnStateChanged(object sender, StateChangedEventArgs e)
        {
            this.CurrentState = e.NewState;
        }
    }
}