# A ready to use state machine for .NET
Copyright &copy; 2020 Frank Listing

There are a lot of variants known to implement state machines. Most of them merge the code of the state machines behavior together with the functional code. A better solution is to strictly separate the code for the state machines logic from the functional code. An existing state machine component is parametrized to define its behavior. 

This help file includes the documentation of an implemantation of a ready to use FSM (Finite State Machine). Using this state machine is very simple. Just define the states, the transitions and the state actions.

### Basic steps to create a State Machine
1. Model your state machine inside a graphical editor e.g. UML tool or any other applicable graphic tool.
1. Create an instance of the FSM class in your code.
1. Transfer all your transitions from the graphic to the code.
1. Register the action handlers for your states.
1. Start the state machine.

#### Available How to documents:
- [Implementing a simple Finite State Machine.](#how-to:-create-a-simple-state-machine)
- [Implementing a State Machine with composite states.](#how-to:-create-a-state-machine-with-composite-states)
- [Using History States.](#how-to:-use-history-states)
- [Build and run the examples.](#Build-and-run-the-examples)

***

# How to: Create a simple State Machine
This topic shows how to implement a simple Finite State Machine using the StateMachine component. This is done by modelling a single traffic light. 

## Example: The steps to create a simple State Machine.
1. Start with the model of the state machine.  
    ![Simple state machine](doc/Images/traffic_light_simple.png)  
    *A simple traffic light with four states, starting with showing the red light.*

1. Create the states and an object of the state machine class.
    ```C#
    /// <summary>
    ///     The name of the state showing red.
    /// </summary>
    private readonly State<object> showingRed = new State<object>(nameof(TrafficLight.showingRed));

    /// <summary>
    ///     The name of the state showing yellow.
    /// </summary>
    private readonly State<object> showingYellow = new State<object>(nameof(TrafficLight.showingYellow));

    /// <summary>
    ///     The name of the state showing green.
    /// </summary>
    private readonly State<object> showingGreen = new State<object>(nameof(TrafficLight.showingGreen));

    /// <summary>
    ///     The name of the state showing red-yellow.
    /// </summary>
    private readonly State<object> showingRedYellow = new State<object>(nameof(TrafficLight.showingRedYellow));

    /// <summary>
    ///     The state machine handling the traffic light.
    /// </summary>
    private readonly Fsm<object> controller = new Fsm<object>(nameof(TrafficLight.controller));
    ```
1. Define all transitions in the code using the `Transition()` method.  
Register the entry action handlers for all states using the `Entry()` method. For special cases exit and do handlers are also avaialble.  
    ```C#
    this.controller.Initial
        .Transition(FsmEvent.NoEvent, this.showingRed);
    this.showingRed
        .Entry(this.Showing_Red_OnEntry)
        .Transition(this.tick, this.showingRedYellow);
    this.showingYellow
        .Entry(this.Showing_Yellow_OnEntry)
        .Transition(this.tick, this.showingRed);
    this.showingRedYellow
        .Entry(this.Showing_RedYellow_OnEntry)
        .Transition(this.tick, this.showingGreen);
    this.showingGreen
        .Entry(this.Showing_Green_OnEntry)
        .Transition(this.tick, this.showingYellow);
    ```
1. Start the state machine using the `Start()` method. 
    ```C#
    this.controller.Start(string.Empty);
    ```

#### Example (Project "TrafficLightSimple")
In this example a simple traffic light is simulated.  
It consists of a simple window with three circles playing the role of the red, yellow and green light and a button used to send a event to simulate a timer tick.

![Example TrafficLightSimple)](doc/Images/example_traffic_light_simple.png)  
*The application*

```C#
using Common.Mvvm;
using StateMachine;

/// <inheritdoc />
/// <summary>
///     A class controlling the traffic light.
/// </summary>
/// <seealso cref="T:Common.Mvvm.ModelBase" />
public class TrafficLight : ModelBase
{
    /// <summary>
    ///     The name of the state showing red.
    /// </summary>
    private readonly State<object> showingRed = new State<object>(nameof(TrafficLight.showingRed));

    /// <summary>
    ///     The name of the state showing yellow.
    /// </summary>
    private readonly State<object> showingYellow = new State<object>(nameof(TrafficLight.showingYellow));

    /// <summary>
    ///     The name of the state showing green.
    /// </summary>
    private readonly State<object> showingGreen = new State<object>(nameof(TrafficLight.showingGreen));

    /// <summary>
    ///     The name of the state showing red-yellow.
    /// </summary>
    private readonly State<object> showingRedYellow = new State<object>(nameof(TrafficLight.showingRedYellow));

    /// <summary>
    ///     The state machine handling the traffic light.
    /// </summary>
    private readonly Fsm<object> controller = new Fsm<object>(nameof(TrafficLight.controller));

    /// <summary>
    ///     The event used to trigger the state machines.
    /// </summary>
    private readonly FsmEvent tick = new FsmEvent(nameof(TrafficLight.tick));

    /// <summary>
    ///     Initializes a new instance of the <see cref="TrafficLight" /> class.
    /// </summary>
    public TrafficLight() => this.InitializeStateMachine();

    /// <summary>
    ///     Gets the red lamp.
    /// </summary>
    public Lamp Red { get; } = new Lamp();

    /// <summary>
    ///     Gets the yellow lamp.
    /// </summary>
    public Lamp Yellow { get; } = new Lamp();

    /// <summary>
    ///     Gets the green lamp.
    /// </summary>
    public Lamp Green { get; } = new Lamp();

    /// <summary>
    ///     Sends the timer event.
    /// </summary>
    public void SendTimerEvent()
    {
        this.controller.Trigger(this.tick, null);
    }

    /// <summary>
    ///     Initializes the state machine.
    /// </summary>
    private void InitializeStateMachine()
    {
        this.InitializeController();

        this.StartBehavior();
    }

    /// <summary>
    ///     Starts the behavior of the state machine.
    /// </summary>
    private void StartBehavior()
    {
        this.controller.Start(string.Empty);
    }

    /// <summary>
    ///     Initializes the controller.
    /// </summary>
    private void InitializeController()
    {
        this.controller.Initial
            .Transition(FsmEvent.NoEvent, this.showingRed);
        this.showingRed
            .Entry(this.Showing_Red_OnEntry)
            .Transition(this.tick, this.showingRedYellow);
        this.showingYellow
            .Entry(this.Showing_Yellow_OnEntry)
            .Transition(this.tick, this.showingRed);
        this.showingRedYellow
            .Entry(this.Showing_RedYellow_OnEntry)
            .Transition(this.tick, this.showingGreen);
        this.showingGreen
            .Entry(this.Showing_Green_OnEntry)
            .Transition(this.tick, this.showingYellow);
    }

    /// <summary>
    ///     Handles the entry action of the Showing_Yellow state.
    /// </summary>
    /// <param name="data">Data associated to this action.</param>
    private void Showing_Yellow_OnEntry(object data)
    {
        this.SetLamps(false, true, false);
    }

    /// <summary>
    ///     Handles the entry action of the Showing_Red state.
    /// </summary>
    /// <param name="data">Data associated to this action.</param>
    private void Showing_Red_OnEntry(object data)
    {
        this.SetLamps(true, false, false);
    }

    /// <summary>
    ///     Handles the entry action of the Showing_Green state.
    /// </summary>
    /// <param name="data">Data associated to this action.</param>
    private void Showing_Green_OnEntry(object data)
    {
        this.SetLamps(false, false, true);
    }

    /// <summary>
    ///     Handles the entry action of the Showing_RedYellow state.
    /// </summary>
    /// <param name="data">Data associated to this action.</param>
    private void Showing_RedYellow_OnEntry(object data)
    {
        this.SetLamps(true, true, false);
    }

    /// <summary>
    ///     Sets the check marks for the lamps of the traffic light.
    /// </summary>
    /// <param name="red">True to check the red lamp, false otherwise.</param>
    /// <param name="yellow">True to check the yellow lamp, false otherwise.</param>
    /// <param name="green">True to check the green lamp, false otherwise.</param>
    private void SetLamps(bool red, bool yellow, bool green)
    {
        this.Red.IsOn = red;
        this.Yellow.IsOn = yellow;
        this.Green.IsOn = green;
    }
}
```

***

# How to: Create a State Machine with composite states
This topic shows how to implement a Finite State Machine with composite states using the StateMachine component.  
This example extends the previous example by an additional mode: Flashing the yellow light.

## Example: The steps to create a State Machine with composite states.
1. Start with the model of the state machine.  
    The model consists of three state machines in two layers.

    ![Composite state machine)](doc/Images/traffic_light_composite.png)  
    *The state machines hierarchy*

    The main machine has two composite states.  
    ![Main Machine)](doc/Images/traffic_light_main.png)  
    *The main FSM*

    The machine for the night mode (the yellow lamp is flashing). This machine will be started when the main machine changes to the "Night_Mode" state. If this machine ends the main machine changes its state to "Day_Mode".  
    ![Night Mode Machine)](doc/Images/traffic_light_night.png)  
    *The FSM for the night mode*

    The machine for the day mode. This machine will be started when the main machine changes to the "Day_Mode" state. If this machine ends the main machine changes its state to "Night_Mode".  
    ![Day Mode Machine)](doc/Images/traffic_light_day.png)  
    *The FSM for the day mode*

1. Create all necessary states and state machine objects.
    ```C#
    /// <summary>
    ///     The name of the state showing red.
    /// </summary>
    private readonly State<object> showingRed = new State<object>(nameof(TrafficLight.showingRed));

    /// <summary>
    ///     The name of the state showing yellow.
    /// </summary>
    private readonly State<object> showingYellowDay = new State<object>(nameof(TrafficLight.showingYellowDay));

    /// <summary>
    ///     The name of the state showing yellow.
    /// </summary>
    private readonly State<object> showingYellowNight = new State<object>(nameof(TrafficLight.showingYellowNight));

    /// <summary>
    ///     The name of the state showing green.
    /// </summary>
    private readonly State<object> showingGreen = new State<object>(nameof(TrafficLight.showingGreen));

    /// <summary>
    ///     The name of the state showing red-yellow.
    /// </summary>
    private readonly State<object> showingRedYellow = new State<object>(nameof(TrafficLight.showingRedYellow));

    /// <summary>
    ///     The name of the state showing nothing.
    /// </summary>
    private readonly State<object> showingNothing = new State<object>(nameof(TrafficLight.showingNothing));

    /// <summary>
    ///     The name of the state night mode.
    /// </summary>
    private readonly State<object> nightMode = new State<object>(nameof(TrafficLight.nightMode));

    /// <summary>
    ///     The name of the state day mode.
    /// </summary>
    private readonly State<object> dayMode = new State<object>(nameof(TrafficLight.dayMode));

    /// <summary>
    ///     The main state machine of the traffic light.
    /// </summary>
    private readonly Fsm<object> mainController = new Fsm<object>(nameof(TrafficLight.mainController));

    /// <summary>
    ///     The state machine handling the night mode of the traffic light.
    /// </summary>
    private readonly Fsm<object> nightController = new Fsm<object>(nameof(TrafficLight.nightController));

    /// <summary>
    ///     The state machine handling the day working mode of the traffic light.
    /// </summary>
    private readonly Fsm<object> dayController = new Fsm<object>(nameof(TrafficLight.dayController));
    ```
1. Define all transitions in the code using the `Transition()` method.  
Register the action handlers for all states using the `Entry()` method.  
Add the child machines to the composite states using the `Child()` method.  
    ```C#
    this.mainController.Initial
        .Transition(FsmEvent.NoEvent, this.dayMode);
    this.nightMode
        .Transition(FsmEvent.NoEvent, this.dayMode)
        .Child(this.nightController);
    this.dayMode
        .Child(this.dayController)
        .Transition(FsmEvent.NoEvent, this.nightMode);
    ```
    ```C#
    this.nightController.Initial
        .Transition(FsmEvent.NoEvent, this.showingYellowNight);
    this.showingYellowNight
        .Entry(this.Showing_Yellow_OnEntry)
        .Transition(
            this.tick,
            this.GetIsNightMode,
            this.showingNothing)
        .Transition(this.tick, o => !this.GetIsNightMode(o), this.nightController.Final);
    this.showingNothing
        .Entry(this.Showing_Nothing_OnEntry)
        .Transition(this.tick, this.showingYellowNight);
    ```
    ```C#
    this.dayController.Initial
        .Transition(FsmEvent.NoEvent, this.showingRed);
    this.showingRed
        .Entry(this.Showing_Red_OnEntry)
        .Transition(
            this.tick,
            o => !this.GetIsNightMode(o),
            this.showingRedYellow)
        .Transition(this.tick, this.GetIsNightMode, this.dayController.Final);
    this.showingYellowDay
        .Entry(this.Showing_Yellow_OnEntry)
        .Transition(this.tick, this.showingRed);
    this.showingRedYellow
        .Entry(this.Showing_RedYellow_OnEntry)
        .Transition(this.tick, this.showingGreen);
    this.showingGreen
        .Entry(this.Showing_Green_OnEntry)
        .Transition(this.tick, this.showingYellowDay);
    ```
1. Start the state machine using the `Start()` method. 
    ```C#
    this.mainController.Start(string.Empty);
    ```

#### Example (Project "TrafficLight")
In this example a simple traffic light with two modes is simulated.  
It consists of a simple window with three circles playing the role of the red, yellow and green light and a button used to send a event to simulate a timer tick.  
A CheckBox controls whether to use the "Day_Mode" or "Night_Mode".

![Example TrafficLight)](doc/Images/example_traffic_light_composite.png)  
*The application*

For the complete code please have look at the example project.  

***

# How-to: Use History States
The state machine supports the use of history states. Two kinds of history states are available:
- Normal history
- Deep history

To specify history the property ```History``` or ```DeepHistory``` of a state has to be specified as end point of a transition.
![Example History)](doc/Images/example_history.png)  
*History states*

```C#
    this.StateWorking
        .Transition(Events.Break, this.StateHandlingError)
        .Transition(FsmEvent.NoEvent, this.StateFinalizing)
        .Entry(this.WorkingEntry)
        .Child(this.Working.Machine);

    this.StateHandlingError
        .Transition(Events.Restart, this.StateWorking)
        .Transition(Events.Continue, this.StateWorking.History)
        .Transition(Events.ContinueDeep, this.StateWorking.DeepHistory)
        .Transition(Events.Next, this.StateFinalizing)
        .Transition(Events.Break, this.Machine.Final)
        .Entry(this.HandlingErrorEntry);
```

To learn how history works please have a look at the "HistoryExample" example project.

***

# Build and run the examples
Because of C# is strongly bundled with .NET, all projects are provided inside a Visual Studio solution. The project can also be opened and compiled using Visual Studio Code with the C# plugin.  
The state machine DLL is a .NET Standard 2.0 DLL which might be run on every platform having .NET Core installed. The example applications use WPF, so they run on Windows only.  

***
Copyright &copy; 2020 Frank Listing
