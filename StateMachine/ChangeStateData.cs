﻿// -----------------------------------------------------------------------
// <copyright file="ChangeStateData.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    /// <summary>
    ///     A class to store data about a state change.
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    internal sealed class ChangeStateData<T> where T : class
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ChangeStateData{T}" /> class.
        /// </summary>
        /// <param name="handled">A value indicating whether an event was handled.</param>
        /// <param name="endPoint">The new end point to change to.</param>
        public ChangeStateData(bool handled, TransitionEndPoint<T> endPoint)
        {
            this.Handled = handled;
            this.EndPoint = endPoint;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ChangeStateData{T}" /> class.
        /// </summary>
        /// <param name="handled">A value indicating whether an event was handled.</param>
        public ChangeStateData(bool handled)
        {
            this.Handled = handled;
        }

        /// <summary>
        ///     Gets a value indicating whether an event was handled.
        /// </summary>
        public bool Handled { get; }

        /// <summary>
        ///     Gets a reference to the new end point of this transition.
        /// </summary>
        public TransitionEndPoint<T> EndPoint { get; }
    }
}