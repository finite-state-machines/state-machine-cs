﻿// -----------------------------------------------------------------------
// <copyright file="Fsm.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    using System;

    /// <summary>
    ///     Class managing the states of a synchronous FSM (finite state machine).
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    public sealed class Fsm<T> where T : class
    {
        /// <summary>
        ///     Holds the final state with lazy creation.
        /// </summary>
        private readonly Lazy<FinalState<T>> final = new Lazy<FinalState<T>>();

        /// <summary>
        ///     Initializes a new instance of the <see cref="T:StateMachine.Fsm`1" /> class.
        /// </summary>
        /// <param name="name">The name of the FSM.</param>
        public Fsm(string name)
        {
            this.Name = name;
            this.CurrentState = this.Initial;
        }

        /// <summary>
        ///     Informs about a state change.
        /// </summary>
        /// <remarks>
        ///     This event is fired before the OnEntry handler of the state is called.
        ///     It should be used mainly for informational purpose.
        /// </remarks>
        public event EventHandler<StateChangedEventArgs> StateChanged;

        /// <summary>
        ///     Gets the name of the state machine.
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Gets the name of the currently active state.
        /// </summary>
        public State<T> CurrentState { get; private set; }

        /// <summary>
        ///     Gets the initial state.
        /// </summary>
        public State<T> Initial { get; } = new InitialState<T>();

        /// <summary>
        ///     Gets the final state.
        /// </summary>
        public State<T> Final => this.final.Value;

        /// <summary>
        ///     Gets a value indicating whether the automaton is started and has not reached the final state.
        /// </summary>
        public bool IsRunning => this.CurrentState != null && !this.CurrentState.IsInitial && !this.HasFinished;

        /// <summary>
        ///     Gets a value indicating whether the automaton has reached the final state.
        /// </summary>
        public bool HasFinished => this.CurrentState != null && this.CurrentState.IsFinal;

        /// <summary>
        ///     Starts the behavior of the Fsm class. Executes the transition from the start state to the first user defined state.
        /// </summary>
        /// <remarks>This method calls the initial states OnEntry method.</remarks>
        /// <param name="data">The data object.</param>
        public void Start(T data)
        {
            this.CurrentState = this.Initial;
            this.Trigger(FsmEvent.StartEvent, data);
        }

        /// <summary>
        ///     Fires the Do event.
        /// </summary>
        /// <param name="data">The data object.</param>
        public void DoAction(T data)
        {
            this.CurrentState.FireDo(data);
        }

        /// <summary>
        ///     Triggers a transition.
        /// </summary>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>
        ///     Returns true if the event was handled, false otherwise. In case of
        ///     asynchronous processing it returns null.
        /// </returns>
        public bool Trigger(FsmEvent trigger, T data)
        {
            Fsm<T>.CheckParameter(trigger);
            if (this.CurrentState == null)
            {
                return false;
            }

            var changeStateData = this.CurrentState.Trigger(trigger, data);
            this.ActivateState(changeStateData, data);

            return changeStateData.Handled;
        }

        /// <summary>
        ///     Checks whether the provided parameter is valid.
        /// </summary>
        /// <param name="trigger">The event parameter to check.</param>
        internal static void CheckParameter(FsmEvent trigger)
        {
            if (trigger == null || trigger == FsmEvent.NoEvent)
            {
                throw new FsmException("Fsm.Trigger: A trigger event cannot be null or NoEvent!");
            }
        }

        /// <summary>
        ///     Activates the new state.
        /// </summary>
        /// <param name="changeStateData">The data needed to activate the next state.</param>
        /// <param name="data">The data.</param>
        private void ActivateState(ChangeStateData<T> changeStateData, T data)
        {
            if (changeStateData.EndPoint?.State == null)
            {
                return;
            }

            var oldStateName = this.CurrentState.Name;
            this.CurrentState = changeStateData.EndPoint.State;

            this.RaiseStateChanged(oldStateName, this.CurrentState.Name);

            this.CurrentState.Start(data, changeStateData.EndPoint.History);
        }

        /// <summary>
        ///     Raises the state changed event.
        /// </summary>
        /// <param name="oldStateName">The name of the old state.</param>
        /// <param name="newStateName">The name of the new state.</param>
        private void RaiseStateChanged(string oldStateName, string newStateName)
        {
            this.StateChanged?.Invoke(this, new StateChangedEventArgs(oldStateName, newStateName));
        }
    }
}