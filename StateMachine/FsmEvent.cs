﻿// -----------------------------------------------------------------------
// <copyright file="FsmEvent.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    /// <summary>
    ///     Class representing an event.
    /// </summary>
    public sealed class FsmEvent
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FsmEvent" /> class.
        /// </summary>
        /// <param name="name">The name of this event (only to have a name for diagnostics and debugging).</param>
        public FsmEvent(string name)
        {
            this.Name = name;
        }

        /// <summary>
        ///     Gets the constant used to specify that no event is necessary.
        /// </summary>
        public static FsmEvent NoEvent { get; } = new FsmEvent("None");

        /// <summary>
        ///     Gets the name of this event (only to have a name for diagnostics and debugging).
        /// </summary>
        public string Name { get; }

        /// <summary>
        ///     Gets the Event used to start the behavior of a FSM.
        /// </summary>
        internal static FsmEvent StartEvent { get; } = new FsmEvent("Start");
    }
}