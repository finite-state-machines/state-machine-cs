﻿// -----------------------------------------------------------------------
// <copyright file="FsmException.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <inheritdoc />
    /// <summary>
    ///     Exception class used with the FSM.
    /// </summary>
    // ReSharper disable UnusedMember.Global
    [Serializable]
    public class FsmException : Exception
    {
        /// <summary>
        ///     The key for the state name.
        /// </summary>
        private const string StateKey = "State";

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class.
        /// </summary>
        /// <remarks>Default initialization necessary for serialization.</remarks>
        public FsmException()
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public FsmException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception.
        ///     If the innerException parameter is not a null reference, the current exception is
        ///     raised in a catch block that handles the inner exception.
        /// </param>
        public FsmException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="stateName">The state which throws this exception.</param>
        public FsmException(string message, string stateName)
            : this(message, stateName, null)
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="stateName">The name of the state which throws this exception.</param>
        /// <param name="innerException">
        ///     The exception that is the cause of the current exception.
        ///     If the innerException parameter is not a null reference, the current exception is
        ///     raised in a catch block that handles the inner exception.
        /// </param>
        public FsmException(string message, string stateName, Exception innerException)
            : base(message, innerException)
        {
            this.StateName = stateName;
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the FsmException class with serialized data.
        /// </summary>
        /// <param name="info">The SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The StreamingContext that contains contextual information about the source or destination.</param>
        protected FsmException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this.StateName = info.GetString(FsmException.StateKey);
        }

        /// <summary>
        ///     Gets the name of the state which throws this exception.
        /// </summary>
        public string StateName { get; }

        /// <inheritdoc />
        /// <summary>
        ///     GetObjectData performs a custom serialization.
        /// </summary>
        /// <param name="info">The SerializationInfo that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The StreamingContext that contains contextual information about the source or destination.</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(FsmException.StateKey, this.StateName);

            base.GetObjectData(info, context);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Returns a String that represents the current Object.
        /// </summary>
        /// <returns>A String that represents the current Object.</returns>
        public override string ToString() => $"{base.ToString()}\n Current state: {this.StateName}";
    }
}