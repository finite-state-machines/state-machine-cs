﻿// -----------------------------------------------------------------------
// <copyright file="History.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    /// <summary>
    ///     Helper class to have a well defined type to support history.
    /// </summary>
    public sealed class History
    {
        /// <summary>
        ///     The type of history represented by this object.
        /// </summary>
        private readonly HistoryType history;

        /// <summary>
        ///     Initializes a new instance of the <see cref="History" /> class (not using history).
        /// </summary>
        public History() : this(HistoryType.None)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="History" /> class.
        /// </summary>
        /// <param name="history">The history value to store.</param>
        /// <remarks>This method is private to prevent the user of providing an invalid enum value.</remarks>
        private History(HistoryType history)
        {
            this.history = history;
        }

        /// <summary>
        ///     The possible history types.
        /// </summary>
        private enum HistoryType
        {
            /// <summary>
            ///     The identifier for no history.
            /// </summary>
            None,

            /// <summary>
            ///     The identifier for normal history.
            /// </summary>
            History,

            /// <summary>
            ///     The identifier for deep history.
            /// </summary>
            DeepHistory,
        }

        /// <summary>
        ///     Gets an instance representing history.
        /// </summary>
        public static History H => new History(HistoryType.History);

        /// <summary>
        ///     Gets an instance representing deep history.
        /// </summary>
        public static History Hx => new History(HistoryType.DeepHistory);

        /// <summary>
        ///     Gets a value indicating whether this instance represents history.
        /// </summary>
        public bool IsHistory => this.history == HistoryType.History;

        /// <summary>
        ///     Gets a value indicating whether this instance represents deep history.
        /// </summary>
        public bool IsDeepHistory => this.history == HistoryType.DeepHistory;
    }
}