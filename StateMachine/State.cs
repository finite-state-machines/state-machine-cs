﻿// -----------------------------------------------------------------------
// <copyright file="State.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     This class is the non generic base of a particular state of the state machine.
    /// </summary>
    public abstract class State
    {
        /// <summary>
        ///     A constant used to identify the initial state of the state machine.
        /// </summary>
        public const string InitialStateName = "Initial";

        /// <summary>
        ///     A constant used to identify the final state of the state machine.
        /// </summary>
        public const string FinalStateName = "Final";

        /// <summary>
        ///     Initializes a new instance of the <see cref="State" /> class.
        /// </summary>
        /// <param name="name">The name of the state.</param>
        protected State(string name)
        {
            this.Name = name;
        }

        /// <summary>
        ///     Gets the name of the state.
        /// </summary>
        public string Name { get; }
    }

    /// <summary>
    ///     This class represents a particular state of the state machine.
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    public class State<T> : State where T : class
    {
        /// <summary>
        ///     The list of currently working child state machines.
        /// </summary>
        private readonly List<Fsm<T>> activeChildren = new List<Fsm<T>>();

        /// <summary>
        ///     The list of all child state machines.
        /// </summary>
        private readonly List<Fsm<T>> children = new List<Fsm<T>>();

        /// <summary>
        ///     Map storing the transition information.
        /// </summary>
        private readonly List<Transition<T>> transitions = new List<Transition<T>>();

        /// <summary>
        ///     The type of the state.
        /// </summary>
        private readonly StateType type;

        /// <summary>
        ///     Initializes a new instance of the <see cref="State{T}" /> class.
        /// </summary>
        /// <param name="name">The name of the state.</param>
        public State(string name) : this(name, StateType.Normal)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="State{T}" /> class.
        /// </summary>
        /// <param name="name">The name of the state.</param>
        /// <param name="type">The type of the state.</param>
        protected State(string name, StateType type) : base(name)
        {
            this.type = type;
        }

        /// <summary>
        ///     Possible state types.
        /// </summary>
        protected enum StateType
        {
            /// <summary>
            ///     Marks a normal state.
            /// </summary>
            Normal,

            /// <summary>
            ///     Marks the initial pseudo state.
            /// </summary>
            Initial,

            /// <summary>
            ///     Marks the final pseudo state.
            /// </summary>
            Final,
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is the initial state.
        /// </summary>
        public bool IsInitial => this.type == StateType.Initial;

        /// <summary>
        ///     Gets a value indicating whether this instance is the final state.
        /// </summary>
        public bool IsFinal => this.type == StateType.Final;

        /// <summary>
        ///     Gets the history state for this state.
        /// </summary>
        public TransitionEndPoint<T> DeepHistory => new TransitionEndPoint<T>(this, StateMachine.History.Hx);

        /// <summary>
        ///     Gets the history state for this state.
        /// </summary>
        public TransitionEndPoint<T> History => new TransitionEndPoint<T>(this, StateMachine.History.H);

        /// <summary>
        ///     Gets or sets the handler method for the states entry action.
        /// </summary>
        private Action<T> OnEntry { get; set; }

        /// <summary>
        ///     Gets or sets the handler method for the states exit action.
        /// </summary>
        private Action<T> OnExit { get; set; }

        /// <summary>
        ///     Gets or sets the handler method for the reaction in the state.
        /// </summary>
        private Action<T> OnDo { get; set; }

        /// <summary>
        ///     Gets a value indicating whether this state has active child machines.
        /// </summary>
        private bool HasActiveChildren => this.activeChildren.Count > 0;

        /// <summary>
        ///     Calls the OnEntry handler of this state and starts all child FSMs if there are some.
        /// </summary>
        /// <param name="data">The data object.</param>
        /// <param name="history">The kind of history to use.</param>
        public void Start(T data, History history)
        {
            if (history.IsHistory && this.TryStartHistory(data))
            {
                return;
            }

            if (history.IsDeepHistory && this.TryStartDeepHistory())
            {
                return;
            }

            this.FireOnEntry(data);
            this.StartChildren(data);
        }

        /// <summary>
        /// Let all direct child FSMs continue working. Calls <see cref="Start" /> if there is no active child.
        /// </summary>
        /// <param name="data">The data object.</param>
        /// <returns>Returns a value indicating whether the start was successful.</returns>
        public bool TryStartHistory(T data)
        {
            if (!this.HasActiveChildren)
            {
                return false;
            }

            this.children.ForEach(child => child.CurrentState.Start(data, new History()));

            return true;
        }

        /// <summary>
        ///     Let all child FSMs continue working. Calls <see cref="Start" /> if there is no active child.
        /// </summary>
        /// <returns>Returns a value indicating whether the start was successful.</returns>
        public bool TryStartDeepHistory()
        {
            return this.HasActiveChildren;
        }

        /// <summary>
        ///     Adds a new child machine.
        /// </summary>
        /// <param name="fsm">The child to add.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Child(Fsm<T> fsm)
        {
            this.CheckSpecialStateUsage("Children");
            this.children.Add(fsm);
            return this;
        }

        /// <summary>
        ///     Sets the handler method for the states entry action.
        /// </summary>
        /// <param name="action">The handler method for the states entry action.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Entry(Action<T> action)
        {
            this.CheckSpecialStateUsage("Actions");
            this.OnEntry = action;
            return this;
        }

        /// <summary>
        ///     Sets the handler method for the states exit action.
        /// </summary>
        /// <param name="action">The handler method for the states exit action.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Exit(Action<T> action)
        {
            this.CheckSpecialStateUsage("Actions");
            this.OnExit = action;
            return this;
        }

        /// <summary>
        ///     Sets the handler method for the reaction in the state.
        /// </summary>
        /// <param name="action">The handler method for the states do action.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Do(Action<T> action)
        {
            this.CheckSpecialStateUsage("Actions");
            this.OnDo = action;
            return this;
        }

        /// <summary>
        ///     Adds a new transition to the state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="stateTo">A reference to the end point of this transition.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Transition(FsmEvent trigger, State<T> stateTo) =>
            this.Transition(trigger, null, stateTo);

        /// <summary>
        ///     Adds a new transition to the state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="condition">Condition handler of this transition.</param>
        /// <param name="stateTo">A reference to the end point of this transition.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Transition(FsmEvent trigger, Func<T, bool> condition, State<T> stateTo)
        {
            this.AddTransition(trigger, condition, new TransitionEndPoint<T>(stateTo));
            return this;
        }

        /// <summary>
        ///     Adds a new transition to the state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="endPoint">A reference to the end point of this transition.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Transition(FsmEvent trigger, TransitionEndPoint<T> endPoint) =>
            this.Transition(trigger, null, endPoint);

        /// <summary>
        ///     Adds a new transition to the state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="condition">Condition handler of this transition.</param>
        /// <param name="endPoint">A reference to the end point of this transition.</param>
        /// <returns>Returns a reference to the state object to support method chaining.</returns>
        public State<T> Transition(FsmEvent trigger, Func<T, bool> condition, TransitionEndPoint<T> endPoint)
        {
            this.AddTransition(trigger, condition, endPoint);
            return this;
        }

        /// <summary>
        ///     Returns a String that represents the current Object.
        /// </summary>
        /// <returns>A String that represents the current Object.</returns>
        public override string ToString() => this.Name;

        /// <summary>
        ///     Fires the Do event.
        /// </summary>
        /// <param name="data">The data object.</param>
        public void FireDo(T data)
        {
            this.activeChildren.ForEach(child => child.DoAction(data));
            this.Fire(this.OnDo, data, nameof(State<T>.OnDo));
        }

        /// <summary>
        ///     Triggers a transition.
        /// </summary>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>Returns the data needed to proceed with the new state.</returns>
        internal ChangeStateData<T> Trigger(FsmEvent trigger, T data)
        {
            return this.ProcessChildren(ref trigger, data)
                ? new ChangeStateData<T>(true)
                : this.ProcessTransitions(trigger, data);
        }

        /// <summary>
        ///     Adds a new transition to the state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="condition">Condition handler of this transition.</param>
        /// <param name="endPoind">A reference to the end point of this transition.</param>
        private void AddTransition(FsmEvent trigger, Func<T, bool> condition, TransitionEndPoint<T> endPoind)
        {
            this.CheckSpecialStateTransitionUsage(endPoind.State);
            var triggerToUse = this.HandleStartState(trigger, condition);

            var transition = new Transition<T>(triggerToUse, condition, endPoind);
            this.transitions.Add(transition);
        }

        /// <summary>
        ///     Starts all registered sub state machines.
        /// </summary>
        /// <param name="data">The data object.</param>
        private void StartChildren(T data)
        {
            this.activeChildren.Clear();
            this.children.ForEach(child => this.StartChild(child, data));
        }

        /// <summary>
        ///     Starts the specified child machine.
        /// </summary>
        /// <param name="child">The child machine to start.</param>
        /// <param name="data">The data object.</param>
        private void StartChild(Fsm<T> child, T data)
        {
            this.activeChildren.Add(child);
            child.Start(data);
        }

        /// <summary>
        ///     Handles the start state.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="condition">Condition handler of this transition.</param>
        /// <returns>Returns the trigger to store in the transition table. In case of the start state the event will be changed.</returns>
        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
        private FsmEvent HandleStartState(FsmEvent trigger, Func<T, bool> condition)
        {
            if (!this.IsInitial)
            {
                return trigger;
            }

            if (this.transitions.Any(transition => transition.Trigger == FsmEvent.StartEvent) || condition != null)
            {
                throw new FsmException(
                    "The start state must have only one transition without an event and without a condition!",
                    this.Name);
            }

            return FsmEvent.StartEvent;
        }

        /// <summary>
        ///     Checks the special state usage.
        /// </summary>
        /// <param name="stateTo">A reference to the end point of the transition.</param>
        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
        private void CheckSpecialStateTransitionUsage(State<T> stateTo)
        {
            if (stateTo == null)
            {
                throw new FsmException("The end point of a transition cannot be null!", this.Name);
            }

            if (this.IsFinal)
            {
                throw new FsmException("The final state cannot be the start point of a transition!", this.Name);
            }

            if (stateTo.IsInitial)
            {
                throw new FsmException("The start state cannot be the end point of a transition!", this.Name);
            }
        }

        /// <summary>
        ///     Checks special states for action usage.
        /// </summary>
        /// <param name="errorLocation">The location of the error.</param>
        /// <exception cref="FsmException">
        ///     The start state cannot have any {errorLocation}
        ///     or
        ///     The final state cannot have any {errorLocation}
        /// </exception>
        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
        private void CheckSpecialStateUsage(string errorLocation)
        {
            if (this.IsInitial)
            {
                throw new FsmException($"The start state cannot have any {errorLocation}!", this.Name);
            }

            if (this.IsFinal)
            {
                throw new FsmException($"The final state cannot have any {errorLocation}!", this.Name);
            }
        }

        /// <summary>
        ///     Processes the transitions.
        /// </summary>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>Returns the data needed to proceed with the new state.</returns>
        private ChangeStateData<T> ProcessTransitions(FsmEvent trigger, T data)
        {
            foreach (var transition in this.transitions.Where(
                transition => transition.Trigger == trigger && transition.Condition(data)))
            {
                return this.ChangeState(transition, data);
            }

            return new ChangeStateData<T>(false);
        }

        /// <summary>
        ///     Processes the child machines.
        /// </summary>
        /// <remarks>
        ///     If the last child machine went to the final state, the <paramref name="trigger" /> is changed to
        ///     <see cref="FsmEvent.NoEvent" /> and the method returns <c>false</c>.
        /// </remarks>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>Returns <c>true</c> if the event was handled, <c>false</c> otherwise.</returns>
        private bool ProcessChildren(ref FsmEvent trigger, T data)
        {
            if (!this.HasActiveChildren)
            {
                return false;
            }

            var handled = this.TriggerChildren(trigger, data);
            if (handled)
            {
                return true;
            }

            if (!this.HasActiveChildren)
            {
                trigger = FsmEvent.NoEvent;
            }

            return false;
        }

        /// <summary>
        ///     Changes the state to the one stored in the transition object.
        /// </summary>
        /// <param name="transition">The actual transition.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>
        ///     Returns the data needed to proceed with the new state.
        /// </returns>
        private ChangeStateData<T> ChangeState(Transition<T> transition, T data)
        {
            this.FireOnExit(data);
            return new ChangeStateData<T>(!transition.IsToFinal, transition.EndPoint);
        }

        /// <summary>
        ///     Triggers the child machines.
        /// </summary>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>Returns <c>true</c> if the event was handled, <c>false</c> otherwise.</returns>
        private bool TriggerChildren(FsmEvent trigger, T data)
        {
            var handled = false;
            this.activeChildren.ToList().ForEach(child => this.TriggerChild(child, trigger, data, ref handled));

            return handled;
        }

        /// <summary>
        ///     Triggers the specified child machine.
        /// </summary>
        /// <param name="child">The child machine to trigger.</param>
        /// <param name="trigger">The event occurred.</param>
        /// <param name="data">The data object.</param>
        /// <param name="handled">Set to <c>true</c> if the event was handled.</param>
        private void TriggerChild(Fsm<T> child, FsmEvent trigger, T data, ref bool handled)
        {
            handled |= child.Trigger(trigger, data);

            if (child.HasFinished)
            {
                this.activeChildren.Remove(child);
            }
        }

        /// <summary>
        ///     Fires the OnEntry event.
        /// </summary>
        /// <param name="data">The data object.</param>
        private void FireOnEntry(T data) => this.Fire(this.OnEntry, data, nameof(State<T>.OnEntry));

        /// <summary>
        ///     Fires the OnExit event.
        /// </summary>
        /// <param name="data">The data object.</param>
        private void FireOnExit(T data) => this.Fire(this.OnExit, data, nameof(State<T>.OnExit));

        /// <summary>
        ///     Calls one of the actions of the state
        /// </summary>
        /// <param name="handler">Action to perform.</param>
        /// <param name="data">The data object.</param>
        /// <param name="action">The name of the action for error processing.</param>
        private void Fire(Action<T> handler, T data, string action)
        {
            try
            {
                handler?.Invoke(data);
            }
            catch (Exception ex)
            {
                var message = $"Error calling the {action} action on method: {handler?.Method.Name}";
                throw new FsmException(message, this.Name, ex);
            }
        }
    }

    /// <summary>
    ///     A class to model the special state 'initial'.
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    /// <seealso cref="StateMachine.State{T}" />
    internal sealed class InitialState<T> : State<T> where T : class
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="InitialState{T}" /> class.
        /// </summary>
        public InitialState() : base(State.InitialStateName, StateType.Initial)
        {
        }
    }

    /// <summary>
    ///     A class to model the special state 'final'.
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    /// <seealso cref="StateMachine.State{T}" />
    internal sealed class FinalState<T> : State<T> where T : class
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="FinalState{T}" /> class.
        /// </summary>
        public FinalState() : base(State.FinalStateName, StateType.Final)
        {
        }
    }
}