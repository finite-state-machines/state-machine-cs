﻿// -----------------------------------------------------------------------
// <copyright file="StateChangedEventArgs.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    using System;

    /// <summary>
    ///     Argument data used to inform about a state change.
    /// </summary>
    public class StateChangedEventArgs : EventArgs
    {
        /// <inheritdoc />
        /// <summary>
        ///     Initializes a new instance of the StateChangedEventArgs class.
        /// </summary>
        /// <param name="oldState">The state before the state change.</param>
        /// <param name="newState">The state after the state change (the current state).</param>
        public StateChangedEventArgs(string oldState, string newState)
        {
            this.OldState = oldState;
            this.NewState = newState;
        }

        /// <summary>
        ///     Gets the state before the state change.
        /// </summary>
        public string OldState { get; }

        /// <summary>
        ///     Gets the state after the state change (the current state).
        /// </summary>
        public string NewState { get; }
    }
}