﻿// -----------------------------------------------------------------------
// <copyright file="Transition.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    using System;

    /// <summary>
    ///     Class holding all information about a transition.
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    internal sealed class Transition<T> where T : class
    {
        /// <summary>
        ///     The condition handler of this transition.
        /// </summary>
        private readonly Func<T, bool> condition;

        /// <summary>
        ///     Initializes a new instance of the Transition class.
        /// </summary>
        /// <param name="trigger">The Event that initiates this transition.</param>
        /// <param name="condition">Condition handler of this transition.</param>
        /// <param name="endPoint">A reference to the end point of this transition.</param>
        public Transition(FsmEvent trigger, Func<T, bool> condition, TransitionEndPoint<T> endPoint)
        {
            this.Trigger = trigger;
            this.condition = condition;
            this.EndPoint = endPoint;
        }

        /// <summary>
        ///     Gets the Event that initiates this transition.
        /// </summary>
        public FsmEvent Trigger { get; }

        /// <summary>
        ///     Gets a reference to the end point of this transition.
        /// </summary>
        public TransitionEndPoint<T> EndPoint { get; }

        /// <summary>
        ///     Gets a value indicating whether the end point is the final state.
        /// </summary>
        public bool IsToFinal => this.EndPoint?.State?.IsFinal == true;

        /// <summary>
        ///     Executes the condition handler of this transition.
        /// </summary>
        /// <param name="data">The data provided to the condition and action handlers.</param>
        /// <returns>
        ///     If there is no condition handler -> returns <c>true</c>. If there is a condition handler, take it's return
        ///     value.
        /// </returns>
        public bool Condition(T data) => this.condition == null || this.condition(data);
    }
}