﻿// -----------------------------------------------------------------------
// <copyright file="TransitionEndPoint.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace StateMachine
{
    /// <summary>
    ///     A class to store the end point of a transition (state and history).
    /// </summary>
    /// <typeparam name="T">The type of data provided to the condition and action handlers.</typeparam>
    public sealed class TransitionEndPoint<T> where T : class
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionEndPoint{T}" /> class.
        /// </summary>
        /// <param name="state">The destination state of the transition.</param>
        public TransitionEndPoint(State<T> state) : this(state, new History())
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="TransitionEndPoint{T}" /> class.
        /// </summary>
        /// <param name="state">The destination state of the transition.</param>
        /// <param name="history">The type of history to use.</param>
        public TransitionEndPoint(State<T> state, History history)
        {
            this.State = state;
            this.History = history;
        }

        /// <summary>
        ///     Gets the destination state of the transition.
        /// </summary>
        public State<T> State { get; }

        /// <summary>
        ///     Gets the type of history to use, the default is none.
        /// </summary>
        public History History { get; }
    }
}