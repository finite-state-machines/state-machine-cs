﻿// -----------------------------------------------------------------------
// <copyright file="TrafficLightControl.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLight.Controls
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    ///     Interaction logic for <see cref="TrafficLightControl"/> class.
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class TrafficLightControl : UserControl
    {
        /// <summary>
        ///     Dependency property to get or set a value indicating whether the red lamp is switched on.
        /// </summary>
        public static readonly DependencyProperty IsRedOnProperty = DependencyProperty.Register(
            nameof(TrafficLightControl.IsRedOn),
            typeof(bool),
            typeof(TrafficLightControl),
            new PropertyMetadata(default(bool)));

        /// <summary>
        ///     Dependency property to get or set a value indicating whether the yellow lamp is switched on.
        /// </summary>
        public static readonly DependencyProperty IsYellowOnProperty = DependencyProperty.Register(
            nameof(TrafficLightControl.IsYellowOn),
            typeof(bool),
            typeof(TrafficLightControl),
            new PropertyMetadata(default(bool)));

        /// <summary>
        ///     Dependency property to get or set a value indicating whether the green lamp is switched on.
        /// </summary>
        public static readonly DependencyProperty IsGreenOnProperty = DependencyProperty.Register(
            nameof(TrafficLightControl.IsGreenOn),
            typeof(bool),
            typeof(TrafficLightControl),
            new PropertyMetadata(default(bool)));

        /// <summary>
        ///     Initializes a new instance of the <see cref="TrafficLightControl" /> class.
        /// </summary>
        public TrafficLightControl() => this.InitializeComponent();

        /// <summary>
        ///     Gets or sets a value indicating whether the red lamp is switched on.
        /// </summary>
        public bool IsRedOn
        {
            get => (bool)this.GetValue(TrafficLightControl.IsRedOnProperty);
            set => this.SetValue(TrafficLightControl.IsRedOnProperty, value);
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the yellow lamp is switched on.
        /// </summary>
        public bool IsYellowOn
        {
            get => (bool)this.GetValue(TrafficLightControl.IsYellowOnProperty);
            set => this.SetValue(TrafficLightControl.IsYellowOnProperty, value);
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the green lamp is switched on.
        /// </summary>
        public bool IsGreenOn
        {
            get => (bool)this.GetValue(TrafficLightControl.IsGreenOnProperty);
            set => this.SetValue(TrafficLightControl.IsGreenOnProperty, value);
        }
    }
}