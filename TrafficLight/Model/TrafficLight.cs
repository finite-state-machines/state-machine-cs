﻿// -----------------------------------------------------------------------
// <copyright file="TrafficLight.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLight.Model
{
    using Common.Mvvm;
    using StateMachine;

    /// <inheritdoc />
    /// <summary>
    ///     A class controlling the traffic light.
    /// </summary>
    /// <seealso cref="T:Common.Mvvm.ModelBase" />
    public class TrafficLight : ModelBase
    {
        /// <summary>
        ///     The name of the state showing red.
        /// </summary>
        private readonly State<object> showingRed = new State<object>(nameof(TrafficLight.showingRed));

        /// <summary>
        ///     The name of the state showing yellow.
        /// </summary>
        private readonly State<object> showingYellowDay = new State<object>(nameof(TrafficLight.showingYellowDay));

        /// <summary>
        ///     The name of the state showing yellow.
        /// </summary>
        private readonly State<object> showingYellowNight = new State<object>(nameof(TrafficLight.showingYellowNight));

        /// <summary>
        ///     The name of the state showing green.
        /// </summary>
        private readonly State<object> showingGreen = new State<object>(nameof(TrafficLight.showingGreen));

        /// <summary>
        ///     The name of the state showing red-yellow.
        /// </summary>
        private readonly State<object> showingRedYellow = new State<object>(nameof(TrafficLight.showingRedYellow));

        /// <summary>
        ///     The name of the state showing nothing.
        /// </summary>
        private readonly State<object> showingNothing = new State<object>(nameof(TrafficLight.showingNothing));

        /// <summary>
        ///     The name of the state night mode.
        /// </summary>
        private readonly State<object> nightMode = new State<object>(nameof(TrafficLight.nightMode));

        /// <summary>
        ///     The name of the state day mode.
        /// </summary>
        private readonly State<object> dayMode = new State<object>(nameof(TrafficLight.dayMode));

        /// <summary>
        ///     The main state machine of the traffic light.
        /// </summary>
        private readonly Fsm<object> mainController = new Fsm<object>(nameof(TrafficLight.mainController));

        /// <summary>
        ///     The state machine handling the night mode of the traffic light.
        /// </summary>
        private readonly Fsm<object> nightController = new Fsm<object>(nameof(TrafficLight.nightController));

        /// <summary>
        ///     The state machine handling the day working mode of the traffic light.
        /// </summary>
        private readonly Fsm<object> dayController = new Fsm<object>(nameof(TrafficLight.dayController));

        /// <summary>
        ///     The event used to trigger the state machines.
        /// </summary>
        private readonly FsmEvent tick = new FsmEvent(nameof(TrafficLight.tick));

        /// <summary>
        ///     A value indicating whether system is in night mode.
        /// </summary>
        private bool isNightMode;

        /// <summary>
        ///     Initializes a new instance of the <see cref="TrafficLight" /> class.
        /// </summary>
        public TrafficLight() => this.InitializeStateMachines();

        /// <summary>
        ///     Gets the red lamp.
        /// </summary>
        public Lamp Red { get; } = new Lamp();

        /// <summary>
        ///     Gets the yellow lamp.
        /// </summary>
        public Lamp Yellow { get; } = new Lamp();

        /// <summary>
        ///     Gets the green lamp.
        /// </summary>
        public Lamp Green { get; } = new Lamp();

        /// <summary>
        ///     Gets or sets a value indicating whether system is in night mode.
        /// </summary>
        public bool IsNightMode
        {
            get => this.isNightMode;
            set => this.SetField(ref this.isNightMode, value);
        }

        /// <summary>
        ///     Sends the timer event.
        /// </summary>
        public void SendTimerEvent()
        {
            this.mainController.Trigger(this.tick, null);
        }

        /// <summary>
        ///     Initializes the state machines.
        /// </summary>
        private void InitializeStateMachines()
        {
            this.InitializeMainController();
            this.InitializeNightController();
            this.InitializeDayController();

            this.StartBehavior();
        }

        /// <summary>
        ///     Starts the behavior of the state machines.
        /// </summary>
        private void StartBehavior()
        {
            this.mainController.Start(string.Empty);
        }

        /// <summary>
        ///     Initializes the day controller (normal working mode).
        /// </summary>
        private void InitializeDayController()
        {
            this.dayController.Initial
                .Transition(FsmEvent.NoEvent, this.showingRed);
            this.showingRed
                .Entry(this.Showing_Red_OnEntry)
                .Transition(
                    this.tick,
                    o => !this.GetIsNightMode(o),
                    this.showingRedYellow)
                .Transition(this.tick, this.GetIsNightMode, this.dayController.Final);
            this.showingYellowDay
                .Entry(this.Showing_Yellow_OnEntry)
                .Transition(this.tick, this.showingRed);
            this.showingRedYellow
                .Entry(this.Showing_RedYellow_OnEntry)
                .Transition(this.tick, this.showingGreen);
            this.showingGreen
                .Entry(this.Showing_Green_OnEntry)
                .Transition(this.tick, this.showingYellowDay);
        }

        /// <summary>
        ///     Initializes the night controller (flashing the yellow lamp).
        /// </summary>
        private void InitializeNightController()
        {
            this.nightController.Initial
                .Transition(FsmEvent.NoEvent, this.showingYellowNight);
            this.showingYellowNight
                .Entry(this.Showing_Yellow_OnEntry)
                .Transition(
                    this.tick,
                    this.GetIsNightMode,
                    this.showingNothing)
                .Transition(this.tick, o => !this.GetIsNightMode(o), this.nightController.Final);
            this.showingNothing
                .Entry(this.Showing_Nothing_OnEntry)
                .Transition(this.tick, this.showingYellowNight);
        }

        /// <summary>
        ///     Initializes the main controller.
        /// </summary>
        private void InitializeMainController()
        {
            this.mainController.Initial
                .Transition(FsmEvent.NoEvent, this.dayMode);
            this.nightMode
                .Transition(FsmEvent.NoEvent, this.dayMode)
                .Child(this.nightController);
            this.dayMode
                .Child(this.dayController)
                .Transition(FsmEvent.NoEvent, this.nightMode);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Nothing state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Nothing_OnEntry(object data)
        {
            this.SetLamps(false, false, false);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Yellow state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Yellow_OnEntry(object data)
        {
            this.SetLamps(false, true, false);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Red state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Red_OnEntry(object data)
        {
            this.SetLamps(true, false, false);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Green state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Green_OnEntry(object data)
        {
            this.SetLamps(false, false, true);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_RedYellow state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_RedYellow_OnEntry(object data)
        {
            this.SetLamps(true, true, false);
        }

        /// <summary>
        ///     Condition method to decide whether to switch from or to the night mode.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        /// <returns>True if the blinking mode should be activated, false otherwise.</returns>
        private bool GetIsNightMode(object data) => this.IsNightMode;

        /// <summary>
        ///     Sets the check marks for the lamps of the traffic light.
        /// </summary>
        /// <param name="red">True to check the red lamp, false otherwise.</param>
        /// <param name="yellow">True to check the yellow lamp, false otherwise.</param>
        /// <param name="green">True to check the green lamp, false otherwise.</param>
        private void SetLamps(bool red, bool yellow, bool green)
        {
            this.Red.IsOn = red;
            this.Yellow.IsOn = yellow;
            this.Green.IsOn = green;
        }
    }
}