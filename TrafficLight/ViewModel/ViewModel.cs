﻿// -----------------------------------------------------------------------
// <copyright file="ViewModel.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLight.ViewModel
{
    using System.Windows.Input;
    using Common.Mvvm;
    using Model;

    /// <inheritdoc />
    /// <summary>
    ///     The view model.
    /// </summary>
    /// <seealso cref="T:Common.Mvvm.ModelBase" />
    public class ViewModel : ModelBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ViewModel" /> class.
        /// </summary>
        /// <param name="trafficLight">The traffic light object to connect to.</param>
        public ViewModel(TrafficLight trafficLight)
        {
            this.TrafficLight = trafficLight;

            this.TimerCommand = new DelegateCommand(this.TimerExecuted);
        }

        /// <summary>
        ///     Gets the timer command.
        /// </summary>
        public ICommand TimerCommand { get; }

        /// <summary>
        ///     Gets the traffic light object.
        /// </summary>
        public TrafficLight TrafficLight { get; }

        /// <summary>
        ///     Executed the timer command.
        /// </summary>
        /// <param name="parameter">The parameter of the command.</param>
        private void TimerExecuted(object parameter)
        {
            this.TrafficLight.SendTimerEvent();
        }
    }
}