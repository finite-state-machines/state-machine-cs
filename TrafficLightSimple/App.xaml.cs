﻿// -----------------------------------------------------------------------
// <copyright file="App.xaml.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple
{
    using System.Windows;
    using Model;

    /// <summary>
    ///     Interaction logic for <see cref="App"/> class.
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class App : Application
    {
        /// <summary>
        ///     Gets the view model.
        /// </summary>
        public static ViewModel.ViewModel ViewModel { get; private set; }

        /// <summary>
        ///     Gets the traffic light object.
        /// </summary>
        public TrafficLight TrafficLight { get; } = new TrafficLight();

        /// <summary>Raises the <see cref="E:System.Windows.Application.Startup" /> event.</summary>
        /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs" /> that contains the event data.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            App.ViewModel = new ViewModel.ViewModel(this.TrafficLight);

            base.OnStartup(e);
        }
    }
}