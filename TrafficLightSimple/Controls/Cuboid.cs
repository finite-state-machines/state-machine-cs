﻿// -----------------------------------------------------------------------
// <copyright file="Cuboid.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Media.Media3D;

    /// <inheritdoc />
    /// <summary>
    ///     A class used to display a cuboid.
    /// </summary>
    /// <seealso cref="T:System.Windows.UIElement3D" />
    public class Cuboid : UIElement3D
    {
        /// <summary>
        ///     Dependency property to get or set the with.
        /// </summary>
        public static readonly DependencyProperty WidthProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Width),
                typeof(double),
                typeof(Cuboid),
                new PropertyMetadata(1.0, Cuboid.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the height.
        /// </summary>
        public static readonly DependencyProperty HeightProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Height),
                typeof(double),
                typeof(Cuboid),
                new PropertyMetadata(1.0, Cuboid.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the depth.
        /// </summary>
        public static readonly DependencyProperty DepthProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Depth),
                typeof(double),
                typeof(Cuboid),
                new PropertyMetadata(1.0, Cuboid.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the divider.
        /// </summary>
        public static readonly DependencyProperty DividerProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Divider),
                typeof(int),
                typeof(Cuboid),
                new PropertyMetadata(25, Cuboid.OnDividerChanged));

        /// <summary>
        ///     Dependency property to get or set the material.
        /// </summary>
        public static readonly DependencyProperty MaterialProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Material),
                typeof(Material),
                typeof(Cuboid),
                new PropertyMetadata(default(Material), Cuboid.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the back material.
        /// </summary>
        public static readonly DependencyProperty BackMaterialProperty =
            DependencyProperty.Register(
                nameof(Cuboid.BackMaterial),
                typeof(Material),
                typeof(Cuboid),
                new PropertyMetadata(default(Material), Cuboid.OnValueChanged));

        /// <summary>
        ///     The error text to use for the exception.
        /// </summary>
        private const string ErrorText = "The value must be greater than zero!";

        /// <summary>
        ///     Dependency property to get or set the model.
        /// </summary>
        private static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register(
                nameof(Cuboid.Model),
                typeof(Model3D),
                typeof(Cuboid),
                new PropertyMetadata(Cuboid.OnModelChanged));

        /// <summary>
        ///     Gets or sets the width.
        /// </summary>
        public double Width
        {
            get => (double)this.GetValue(Cuboid.WidthProperty);
            set => this.SetValue(Cuboid.WidthProperty, value);
        }

        /// <summary>
        ///     Gets or sets the height.
        /// </summary>
        public double Height
        {
            get => (double)this.GetValue(Cuboid.HeightProperty);
            set => this.SetValue(Cuboid.HeightProperty, value);
        }

        /// <summary>
        ///     Gets or sets the depth.
        /// </summary>
        public double Depth
        {
            get => (double)this.GetValue(Cuboid.DepthProperty);
            set => this.SetValue(Cuboid.DepthProperty, value);
        }

        /// <summary>
        ///     Gets or sets the divider.
        /// </summary>
        public int Divider
        {
            get => (int)this.GetValue(Cuboid.DividerProperty);
            set => this.SetValue(Cuboid.DividerProperty, value);
        }

        /// <summary>
        ///     Gets or sets the material.
        /// </summary>
        public Material Material
        {
            get => (Material)this.GetValue(Cuboid.MaterialProperty);
            set => this.SetValue(Cuboid.MaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets the back material.
        /// </summary>
        public Material BackMaterial
        {
            get => (Material)this.GetValue(Cuboid.BackMaterialProperty);
            set => this.SetValue(Cuboid.BackMaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets the model.
        /// </summary>
        private Model3D Model
        {
            get => (Model3D)this.GetValue(Cuboid.ModelProperty);
            set => this.SetValue(Cuboid.ModelProperty, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Participates in rendering operations when overridden in a derived class.
        /// </summary>
        /// <remarks>
        ///     OnUpdateModel is called in response to InvalidateModel and provides
        ///     a place to set the Visual3DModel property.
        ///     Setting Visual3DModel does not provide parenting information, which
        ///     is needed for data binding, styling, and other features. Similarly, creating render data
        ///     in 2-D does not provide the connections either.
        ///     To get around this, we create a Model dependency property which
        ///     sets this value.  The Model DP then causes the correct connections to occur
        ///     and the above features to work correctly.
        /// </remarks>
        protected override void OnUpdateModel()
        {
            var model = new GeometryModel3D
            {
                Geometry = this.GenerateGeometry(),
                Material = this.Material,
                BackMaterial = this.BackMaterial
            };

            this.Model = model;
        }

        /// <summary>
        ///     Called when a value was changed. Initiates the rendering process.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnValueChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs args)
        {
            var cuboid = d as Cuboid;
            cuboid?.InvalidateModel();
        }

        /// <summary>
        ///     Called when the divider value has changed.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        /// <exception cref="ArgumentOutOfRangeException">The value must be greater than zero!</exception>
        private static void OnDividerChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if ((int)e.NewValue < 1)
            {
                throw new ArgumentOutOfRangeException(Cuboid.ErrorText);
            }

            Cuboid.OnValueChanged(d, e);
        }

        /// <summary>
        ///     Called when the model has changed.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnModelChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is Cuboid cuboid)
            {
                cuboid.Visual3DModel = cuboid.Model;
            }
        }

        /// <summary>
        ///     Adds a side.
        /// </summary>
        /// <param name="mesh">The mesh.</param>
        /// <param name="leftTop">The left top.</param>
        /// <param name="offsetRight">The offset right.</param>
        /// <param name="offsetDown">The offset down.</param>
        /// <param name="pieces">The number of pieces.</param>
        /// <param name="startIndex">The start index.</param>
        /// <returns>Returns the index of the next triangle.</returns>
        private static int AddSide(
            MeshGeometry3D mesh,
            Point3D leftTop,
            Vector3D offsetRight,
            Vector3D offsetDown,
            int pieces,
            int startIndex)
        {
            var index = startIndex;
            for (var i = 0; i < pieces; i++)
            {
                for (var j = 0; j < pieces; j++)
                {
                    var startCorner = leftTop + (offsetRight * i) + (offsetDown * j);
                    index = Cuboid.AddSideArea(
                        mesh,
                        startCorner,
                        startCorner + offsetRight,
                        startCorner + offsetDown,
                        startCorner + offsetRight + offsetDown,
                        index);
                }
            }

            return index;
        }

        /// <summary>
        ///     Adds a side area.
        /// </summary>
        /// <param name="mesh">The mesh.</param>
        /// <param name="leftTop">The left top.</param>
        /// <param name="rightTop">The right top.</param>
        /// <param name="leftBottom">The left bottom.</param>
        /// <param name="rightBottom">The right bottom.</param>
        /// <param name="startIndex">The start index.</param>
        /// <returns>Returns the index of the next triangle.</returns>
        private static int AddSideArea(
            MeshGeometry3D mesh,
            Point3D leftTop,
            Point3D rightTop,
            Point3D leftBottom,
            Point3D rightBottom,
            int startIndex)
        {
            mesh.Positions.Add(leftTop);
            mesh.Positions.Add(rightTop);
            mesh.Positions.Add(leftBottom);
            mesh.Positions.Add(rightBottom);

            mesh.TriangleIndices.Add(startIndex);
            mesh.TriangleIndices.Add(startIndex + 2);
            mesh.TriangleIndices.Add(startIndex + 1);
            mesh.TriangleIndices.Add(startIndex + 1);
            mesh.TriangleIndices.Add(startIndex + 2);
            mesh.TriangleIndices.Add(startIndex + 3);

            return startIndex + 4;
        }

        /// <summary>
        ///     Generates the positions.
        /// </summary>
        /// <returns>Returns the generated geometry with the calculated positions.</returns>
        private MeshGeometry3D GenerateGeometry()
        {
            var mesh = new MeshGeometry3D();
            var width = new Vector3D(this.Width, 0.0, 0.0);
            var height = new Vector3D(0.0, this.Height, 0.0);
            var depth = new Vector3D(0.0, 0.0, this.Depth);

            this.BuildSides(mesh, width, height, depth);

            return mesh;
        }

        /// <summary>
        ///     Builds the sides of the cuboid.
        /// </summary>
        /// <param name="mesh">The mesh.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="depth">The depth.</param>
        private void BuildSides(MeshGeometry3D mesh, Vector3D width, Vector3D height, Vector3D depth)
        {
            // offsets
            var widthOffset = width / this.Divider;
            var heightOffset = height / this.Divider;
            var depthOffset = depth / this.Divider;

            // points of base area
            var baseLeftTop = new Point3D() - (width / 2) - (height / 2) - (depth / 2);
            var baseRightTop = baseLeftTop + width;
            var baseLeftBottom = baseLeftTop + depth;
            var baseRightBottom = baseRightTop + depth;

            // points of cover area
            var coverLeftTop = baseLeftTop + height;

            var positionIndex = 0;

            // upper side
            positionIndex = Cuboid.AddSide(mesh, coverLeftTop, widthOffset, depthOffset, this.Divider, positionIndex);

            // front side
            positionIndex = Cuboid.AddSide(
                mesh,
                baseRightBottom,
                widthOffset * -1.0,
                heightOffset,
                this.Divider,
                positionIndex);

            // right side
            positionIndex = Cuboid.AddSide(mesh, baseRightTop, depthOffset, heightOffset, this.Divider, positionIndex);

            // back side
            positionIndex = Cuboid.AddSide(mesh, baseLeftTop, widthOffset, heightOffset, this.Divider, positionIndex);

            // left side
            positionIndex = Cuboid.AddSide(
                mesh,
                coverLeftTop,
                depthOffset,
                heightOffset * -1.0,
                this.Divider,
                positionIndex);

            // lower side
            Cuboid.AddSide(mesh, baseLeftBottom, widthOffset, depthOffset * -1.0, this.Divider, positionIndex);

            mesh.Freeze();
        }
    }
}