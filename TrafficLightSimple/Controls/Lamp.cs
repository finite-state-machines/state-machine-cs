﻿// -----------------------------------------------------------------------
// <copyright file="Lamp.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple.Controls
{
    using System.Windows;
    using System.Windows.Media.Media3D;

    /// <inheritdoc />
    /// <summary>
    ///     A class used to display a lamp.
    /// </summary>
    /// <seealso cref="T:TrafficLight.Controls.Sphere" />
    public class Lamp : Sphere
    {
        /// <summary>
        ///     Dependency property to get or set the material to show when the lamp is switched on.
        /// </summary>
        public static readonly DependencyProperty OnMaterialProperty = DependencyProperty.Register(
            nameof(Lamp.OnMaterial),
            typeof(Material),
            typeof(Lamp),
            new PropertyMetadata(default(Material)));

        /// <summary>
        ///     Dependency property to get or set the material to show when the lamp is switched off.
        /// </summary>
        public static readonly DependencyProperty OffMaterialProperty = DependencyProperty.Register(
            nameof(Lamp.OffMaterial),
            typeof(Material),
            typeof(Lamp),
            new PropertyMetadata(default(Material)));

        /// <summary>
        ///     Dependency property to get or set a value indicating whether the lamp is switched on.
        /// </summary>
        public static readonly DependencyProperty IsOnProperty = DependencyProperty.Register(
            nameof(Lamp.IsOn),
            typeof(bool),
            typeof(Lamp),
            new PropertyMetadata(default(bool), Lamp.OnValueChanged));

        /// <summary>
        ///     Gets or sets the material to show when the lamp is switched on.
        /// </summary>
        public Material OnMaterial
        {
            get => (Material)this.GetValue(Lamp.OnMaterialProperty);
            set => this.SetValue(Lamp.OnMaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets the material to show when the lamp is switched off.
        /// </summary>
        public Material OffMaterial
        {
            get => (Material)this.GetValue(Lamp.OffMaterialProperty);
            set => this.SetValue(Lamp.OffMaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the lamp is switched on.
        /// </summary>
        public bool IsOn
        {
            get => (bool)this.GetValue(Lamp.IsOnProperty);
            set => this.SetValue(Lamp.IsOnProperty, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Participates in rendering operations when overridden in a derived class.
        /// </summary>
        /// <remarks>
        ///     OnUpdateModel is called in response to InvalidateModel and provides
        ///     a place to set the Visual3DModel property.
        ///     Setting Visual3DModel does not provide parenting information, which
        ///     is needed for data binding, styling, and other features. Similarly, creating render data
        ///     in 2-D does not provide the connections either.
        ///     To get around this, we create a Model dependency property which
        ///     sets this value.  The Model DP then causes the correct connections to occur
        ///     and the above features to work correctly.
        /// </remarks>
        protected override void OnUpdateModel()
        {
            this.Material = this.IsOn ? this.OnMaterial : this.OffMaterial;
            base.OnUpdateModel();
        }

        /// <summary>
        ///     Called when a value was changed. Initiates the rendering process.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var lamp = d as Lamp;
            lamp?.InvalidateModel();
        }
    }
}