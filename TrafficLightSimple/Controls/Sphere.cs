﻿// -----------------------------------------------------------------------
// <copyright file="Sphere.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Media.Media3D;

    /// <inheritdoc />
    /// <summary>
    ///     A class used to display a sphere.
    /// </summary>
    /// <seealso cref="T:System.Windows.UIElement3D" />
    public class Sphere : UIElement3D
    {
        /// <summary>
        ///     Dependency property to get or set the divider.
        /// </summary>
        public static readonly DependencyProperty DividerProperty =
            DependencyProperty.Register(
                nameof(Sphere.Divider),
                typeof(int),
                typeof(Sphere),
                new PropertyMetadata(15, Sphere.OnDividerChanged));

        /// <summary>
        ///     Dependency property to get or set the radius.
        /// </summary>
        public static readonly DependencyProperty RadiusProperty =
            DependencyProperty.Register(
                nameof(Sphere.Radius),
                typeof(double),
                typeof(Sphere),
                new PropertyMetadata(1.0, Sphere.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the material.
        /// </summary>
        public static readonly DependencyProperty MaterialProperty =
            DependencyProperty.Register(
                nameof(Sphere.Material),
                typeof(Material),
                typeof(Sphere),
                new PropertyMetadata(default(Material), Sphere.OnValueChanged));

        /// <summary>
        ///     Dependency property to get or set the back material.
        /// </summary>
        public static readonly DependencyProperty BackMaterialProperty =
            DependencyProperty.Register(
                nameof(Sphere.BackMaterial),
                typeof(Material),
                typeof(Sphere),
                new PropertyMetadata(default(Material), Sphere.OnValueChanged));

        /// <summary>
        ///     The error text to use for the exception.
        /// </summary>
        private const string ErrorText = "The value must be greater than zero!";

        /// <summary>
        ///     Dependency property to get or set the model.
        /// </summary>
        private static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register(
                nameof(Sphere.Model),
                typeof(Model3D),
                typeof(Sphere),
                new PropertyMetadata(Sphere.OnModelChanged));

        /// <summary>
        ///     Gets or sets the divider.
        /// </summary>
        public int Divider
        {
            get => (int)this.GetValue(Sphere.DividerProperty);
            set => this.SetValue(Sphere.DividerProperty, value);
        }

        /// <summary>
        ///     Gets or sets the radius.
        /// </summary>
        public double Radius
        {
            get => (double)this.GetValue(Sphere.RadiusProperty);
            set => this.SetValue(Sphere.RadiusProperty, value);
        }

        /// <summary>
        ///     Gets or sets the material.
        /// </summary>
        public Material Material
        {
            get => (Material)this.GetValue(Sphere.MaterialProperty);
            set => this.SetValue(Sphere.MaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets the back material.
        /// </summary>
        public Material BackMaterial
        {
            get => (Material)this.GetValue(Sphere.BackMaterialProperty);
            set => this.SetValue(Sphere.BackMaterialProperty, value);
        }

        /// <summary>
        ///     Gets or sets the model.
        /// </summary>
        private Model3D Model
        {
            get => (Model3D)this.GetValue(Sphere.ModelProperty);
            set => this.SetValue(Sphere.ModelProperty, value);
        }

        /// <inheritdoc />
        /// <summary>
        ///     Participates in rendering operations when overridden in a derived class.
        /// </summary>
        /// <remarks>
        ///     OnUpdateModel is called in response to InvalidateModel and provides
        ///     a place to set the Visual3DModel property.
        ///     Setting Visual3DModel does not provide parenting information, which
        ///     is needed for data binding, styling, and other features. Similarly, creating render data
        ///     in 2-D does not provide the connections either.
        ///     To get around this, we create a Model dependency property which
        ///     sets this value.  The Model DP then causes the correct connections to occur
        ///     and the above features to work correctly.
        /// </remarks>
        protected override void OnUpdateModel()
        {
            var model = new GeometryModel3D
            {
                Geometry = Sphere.Tessellate(this.Divider, this.Radius),
                Material = this.Material,
                BackMaterial = this.BackMaterial
            };

            this.Model = model;
        }

        /// <summary>
        ///     Called when a value was changed. Initiates the rendering process.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="args">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnValueChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs args)
        {
            var sphere = d as Sphere;
            sphere?.InvalidateModel();
        }

        /// <summary>
        ///     Called when the model has changed.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        private static void OnModelChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (d is Sphere sphere)
            {
                sphere.Visual3DModel = sphere.Model;
            }
        }

        /// <summary>
        ///     Called when the divider value has changed.
        /// </summary>
        /// <param name="d">The dependency object owning the property.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs" /> instance containing the event data.</param>
        /// <exception cref="ArgumentOutOfRangeException">The value must be greater than zero!</exception>
        private static void OnDividerChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if ((int)e.NewValue < 1)
            {
                throw new ArgumentOutOfRangeException(Sphere.ErrorText);
            }

            Sphere.OnValueChanged(d, e);
        }

        /// <summary>
        ///     Gets the position as a point value.
        /// </summary>
        /// <param name="theta">The theta value.</param>
        /// <param name="phi">The phi value.</param>
        /// <param name="radius">The radius.</param>
        /// <returns>Returns a point calculated from the given values.</returns>
        private static Point3D GetPosition(double theta, double phi, double radius)
        {
            var x = radius * Math.Sin(theta) * Math.Sin(phi);
            var y = radius * Math.Cos(phi);
            var z = radius * Math.Cos(theta) * Math.Sin(phi);

            return new Point3D(x, y, z);
        }

        /// <summary>
        ///     Gets the normal vector.
        /// </summary>
        /// <param name="theta">The theta value.</param>
        /// <param name="phi">The phi value.</param>
        /// <returns>Returns a vector calculated from the given values.</returns>
        private static Vector3D GetNormal(double theta, double phi)
        {
            return (Vector3D)Sphere.GetPosition(theta, phi, 1.0);
        }

        /// <summary>
        ///     Converts degrees to radiant.
        /// </summary>
        /// <param name="degrees">The degrees value.</param>
        /// <returns>Returns the radiant value.</returns>
        private static double DegToRad(double degrees)
        {
            return (degrees / 180.0) * Math.PI;
        }

        /// <summary>
        ///     Gets the texture coordinate.
        /// </summary>
        /// <param name="theta">The theta value.</param>
        /// <param name="phi">The phi value.</param>
        /// <returns>Returns a point calculated from the given values.</returns>
        private static Point GetTextureCoordinate(double theta, double phi)
        {
            var p = new Point(theta / (2 * Math.PI), phi / Math.PI);
            return p;
        }

        /// <summary>
        ///     Tessellates the sphere and returns a <see cref="MeshGeometry3D" /> representing the
        ///     tessellation based on the given parameters.
        /// </summary>
        /// <param name="divider">The divider.</param>
        /// <param name="radius">The radius.</param>
        /// <returns>
        ///     Returns a <see cref="MeshGeometry3D" /> representing the
        ///     tessellation based on the given parameters.
        /// </returns>
        private static MeshGeometry3D Tessellate(int divider, double radius)
        {
            var dt = Sphere.DegToRad(360.0) / divider;
            var dp = Sphere.DegToRad(180.0) / divider;

            var mesh = new MeshGeometry3D();

            for (var pi = 0; pi <= divider; pi++)
            {
                var phi = pi * dp;

                for (var ti = 0; ti <= divider; ti++)
                {
                    // we want to start the mesh on the x axis
                    var theta = ti * dt;

                    mesh.Positions.Add(Sphere.GetPosition(theta, phi, radius));
                    mesh.Normals.Add(Sphere.GetNormal(theta, phi));
                    mesh.TextureCoordinates.Add(Sphere.GetTextureCoordinate(theta, phi));
                }
            }

            for (var pi = 0; pi < divider; pi++)
            {
                for (var ti = 0; ti < divider; ti++)
                {
                    var x0 = ti;
                    var x1 = ti + 1;
                    var y0 = pi * (divider + 1);
                    var y1 = (pi + 1) * (divider + 1);

                    mesh.TriangleIndices.Add(x0 + y0);
                    mesh.TriangleIndices.Add(x0 + y1);
                    mesh.TriangleIndices.Add(x1 + y0);

                    mesh.TriangleIndices.Add(x1 + y0);
                    mesh.TriangleIndices.Add(x0 + y1);
                    mesh.TriangleIndices.Add(x1 + y1);
                }
            }

            mesh.Freeze();
            return mesh;
        }
    }
}