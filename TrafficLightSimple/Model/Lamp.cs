﻿// -----------------------------------------------------------------------
// <copyright file="Lamp.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple.Model
{
    using Common.Mvvm;

    /// <inheritdoc />
    /// <summary>
    ///     A class representing a lamp.
    /// </summary>
    /// <seealso cref="T:Common.Mvvm.ModelBase" />
    public class Lamp : ModelBase
    {
        /// <summary>
        ///     A value indicating whether the lamp is switched on.
        /// </summary>
        private bool isOn;

        /// <summary>
        ///     Gets or sets a value indicating whether the lamp is switched on.
        /// </summary>
        public bool IsOn
        {
            get => this.isOn;
            set => this.SetField(ref this.isOn, value);
        }
    }
}