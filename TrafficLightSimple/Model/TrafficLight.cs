﻿// -----------------------------------------------------------------------
// <copyright file="TrafficLight.cs">
//     Copyright (c) Frank Listing. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TrafficLightSimple.Model
{
    using Common.Mvvm;
    using StateMachine;

    /// <inheritdoc />
    /// <summary>
    ///     A class controlling the traffic light.
    /// </summary>
    /// <seealso cref="T:Common.Mvvm.ModelBase" />
    public class TrafficLight : ModelBase
    {
        /// <summary>
        ///     The name of the state showing red.
        /// </summary>
        private readonly State<object> showingRed = new State<object>(nameof(TrafficLight.showingRed));

        /// <summary>
        ///     The name of the state showing yellow.
        /// </summary>
        private readonly State<object> showingYellow = new State<object>(nameof(TrafficLight.showingYellow));

        /// <summary>
        ///     The name of the state showing green.
        /// </summary>
        private readonly State<object> showingGreen = new State<object>(nameof(TrafficLight.showingGreen));

        /// <summary>
        ///     The name of the state showing red-yellow.
        /// </summary>
        private readonly State<object> showingRedYellow = new State<object>(nameof(TrafficLight.showingRedYellow));

        /// <summary>
        ///     The state machine handling the traffic light.
        /// </summary>
        private readonly Fsm<object> controller = new Fsm<object>(nameof(TrafficLight.controller));

        /// <summary>
        ///     The event used to trigger the state machines.
        /// </summary>
        private readonly FsmEvent tick = new FsmEvent(nameof(TrafficLight.tick));

        /// <summary>
        ///     Initializes a new instance of the <see cref="TrafficLight" /> class.
        /// </summary>
        public TrafficLight() => this.InitializeStateMachine();

        /// <summary>
        ///     Gets the red lamp.
        /// </summary>
        public Lamp Red { get; } = new Lamp();

        /// <summary>
        ///     Gets the yellow lamp.
        /// </summary>
        public Lamp Yellow { get; } = new Lamp();

        /// <summary>
        ///     Gets the green lamp.
        /// </summary>
        public Lamp Green { get; } = new Lamp();

        /// <summary>
        ///     Sends the timer event.
        /// </summary>
        public void SendTimerEvent()
        {
            this.controller.Trigger(this.tick, null);
        }

        /// <summary>
        ///     Initializes the state machine.
        /// </summary>
        private void InitializeStateMachine()
        {
            this.InitializeController();

            this.StartBehavior();
        }

        /// <summary>
        ///     Starts the behavior of the state machine.
        /// </summary>
        private void StartBehavior()
        {
            this.controller.Start(string.Empty);
        }

        /// <summary>
        ///     Initializes the controller.
        /// </summary>
        private void InitializeController()
        {
            this.controller.Initial
                .Transition(FsmEvent.NoEvent, this.showingRed);
            this.showingRed
                .Entry(this.Showing_Red_OnEntry)
                .Transition(this.tick, this.showingRedYellow);
            this.showingYellow
                .Entry(this.Showing_Yellow_OnEntry)
                .Transition(this.tick, this.showingRed);
            this.showingRedYellow
                .Entry(this.Showing_RedYellow_OnEntry)
                .Transition(this.tick, this.showingGreen);
            this.showingGreen
                .Entry(this.Showing_Green_OnEntry)
                .Transition(this.tick, this.showingYellow);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Yellow state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Yellow_OnEntry(object data)
        {
            this.SetLamps(false, true, false);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Red state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Red_OnEntry(object data)
        {
            this.SetLamps(true, false, false);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_Green state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_Green_OnEntry(object data)
        {
            this.SetLamps(false, false, true);
        }

        /// <summary>
        ///     Handles the entry action of the Showing_RedYellow state.
        /// </summary>
        /// <param name="data">Data associated to this action.</param>
        private void Showing_RedYellow_OnEntry(object data)
        {
            this.SetLamps(true, true, false);
        }

        /// <summary>
        ///     Sets the check marks for the lamps of the traffic light.
        /// </summary>
        /// <param name="red">True to check the red lamp, false otherwise.</param>
        /// <param name="yellow">True to check the yellow lamp, false otherwise.</param>
        /// <param name="green">True to check the green lamp, false otherwise.</param>
        private void SetLamps(bool red, bool yellow, bool green)
        {
            this.Red.IsOn = red;
            this.Yellow.IsOn = yellow;
            this.Green.IsOn = green;
        }
    }
}